
## Rust Web Development 
Michael Ermeus

# Objective 

This repo is for me to practice Web Development so bear with me.
Thanks.....

## Comment

I used the api file from the knock knock joke to help me implement the question_api and the datebase
to make the data persistent


# Axum CRUD Application

This is a simple CRUD application built with Axum and PostgreSQL.

## Note: Program Status

Please note that this application is a work in progress and is not fully implemented. As of now, there may be errors due to incomplete database setup and other missing functionalities. 

## How It Works So Far

The application currently provides basic CRUD (Create, Read, Update, Delete) operations for managing questions. Here's a brief overview of how each operation works:

- **Create**: You can create a new question by sending a POST request to `/questions` endpoint with the question details in the request body.
- **Read**: To retrieve a question by its ID, send a GET request to `/questions/:id` where `:id` is the ID of the question you want to retrieve.
- **Update**: Updating a question is done by sending a PUT request to `/questions/:id` with the updated question details in the request body.
- **Delete**: To delete a question, send a DELETE request to `/questions/:id` where `:id` is the ID of the question you want to delete.

## Setup

1. **Clone the repository**:

   ```sh
   git clone https://gitlab.cecs.pdx.edu/miermeus/rust_miermeus
	user will need to create an account with a webhosting service of their choice beacuse *DATABASE URL IS NOT PROVIDED*

