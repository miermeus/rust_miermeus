use axum::{
    extract::{Path, State},
    response::{IntoResponse, Response},
    Json,
};
use hyper::StatusCode;
use serde::{Deserialize, Serialize};
use sqlx::types::Uuid;
use sqlx::{query, query_as, Pool, Postgres};
use utoipa::OpenApi;

#[derive(OpenApi)]
#[openapi(
    paths(
        questions,
        random_question,
        get_question,
        post_question,
        delete_question,
        update_question,
    ),
    components(
        schemas(Question, QuestionBaseError)
    ),
    tags(
        (name = "question", description = "Question API")
    )
)]
pub struct ApiDoc;

#[derive(Serialize, Deserialize, Debug)]
pub struct QuestionBaseError {
    pub message: String,
}

impl QuestionBaseError {
    fn response(status: StatusCode, message: String) -> Response {
        (status, Json(QuestionBaseError { message })).into_response()
    }
}

#[utoipa::path(
    get,
    path = "/api/v1/questions",
    responses(
        (status = 200, description = "List Questions", body = [Question])
    )
)]
pub async fn questions(State(state): State<AppState>) -> Response {
    let questions: Vec<Question> = query_as!(
        Question,
        "SELECT id, title, content, tags FROM questions"
    ).fetch_all(&state.db_pool).await.unwrap_or_else(|_| vec![]);
    (StatusCode::OK, Json(questions)).into_response()
}

#[utoipa::path(
    get,
    path = "/api/v1/question",
    responses(
        (status = 200, description = "Return random question", body = Question),
        (status = 204, description = "Question base is empty", body = QuestionBaseError)
    )
)]

pub async fn random_question(State(state): State<AppState>) -> Response {
    match query_as!(
        Question,
        "SELECT id, title, content, tags FROM questions ORDER BY random() LIMIT 1"
    ).fetch_optional(&state.db_pool).await
    {
        Ok(Some(question)) => question.into_response(),
        Ok(None) => QuestionBaseError::response(StatusCode::NO_CONTENT, "No questions available".to_string()),
        Err(_) => QuestionBaseError::response(StatusCode::INTERNAL_SERVER_ERROR, "Database error".to_string()),
    }
}

#[utoipa::path(
    get,
    path = "/api/v1/question/{id}",
    responses(
        (status = 200, description = "Return specified question", body = Question),
        (status = 404, description = "No question with this ID", body = QuestionBaseError)
    )
)]
pub async fn get_question(State(state): State<AppState>, Path(question_id): Path<Uuid>) -> Response {
    match query_as!(
        Question,
        "SELECT id, title, content, tags FROM questions WHERE id = $1",
        question_id
    ).fetch_optional(&state.db_pool).await
    {
        Ok(Some(question)) => question.into_response(),
        Ok(None) => QuestionBaseError::response(StatusCode::NOT_FOUND, "Question not found".to_string()),
        Err(_) => QuestionBaseError::response(StatusCode::INTERNAL_SERVER_ERROR, "Database error".to_string()),
    }
}

#[utoipa::path(
    post,
    path = "/api/v1/question/add",
    request_body(
        content = inline(Question),
        description = "Question to add"
    ),
    responses(
        (status = 201, description = "Added question", body = ()),
        (status = 400, description = "Bad request", body = QuestionBaseError)
    )
)]
pub async fn post_question(State(state): State<AppState>,Json(question): Json<Question>,
) -> Response {
    let new_question = Question::new(
        Uuid::new_v4(),
        &question.title,
        &question.content,
        &question.tags.iter().map(String::as_str).collect::<Vec<_>>(),
    );

    match query!(
        "INSERT INTO questions (id, title, content, tags) VALUES ($1, $2, $3, $4)",
        new_question.id,
        new_question.title,
        new_question.content,
        new_question.tags
    ).execute(&state.db_pool).await
    {
        Ok(_) => StatusCode::CREATED.into_response(),
        Err(_) => QuestionBaseError::response(StatusCode::BAD_REQUEST, "Failed to add question".to_string()),
    }
}

#[utoipa::path(
    delete,
    path = "/api/v1/question/{id}",
    responses(
        (status = 200, description = "Deleted question", body = ()),
        (status = 400, description = "Bad request", body = QuestionBaseError)
    )
)]
pub async fn delete_question(
    State(state): State<AppState>,
    Path(question_id): Path<Uuid>,
) -> Response {
    match query!("DELETE FROM questions WHERE id = $1", question_id).execute(&state.db_pool).await
    {
        Ok(_) => StatusCode::OK.into_response(),
        Err(_) => QuestionBaseError::response(StatusCode::BAD_REQUEST, "Failed to delete question".to_string()),
    }
}

#[utoipa::path(
    put,
    path = "/api/v1/question/{id}",
    request_body(
        content = inline(Question),
        description = "Question to update"
    ),
    responses(
        (status = 200, description = "Updated question", body = ()),
        (status = 400, description = "Bad request", body = QuestionBaseError),
        (status = 404, description = "Not found", body = QuestionBaseError),
        (status = 422, description = "Unprocessable entity", body = QuestionBaseError)
    )
)]
pub async fn update_question(
    State(state): State<AppState>,
    Path(question_id): Path<Uuid>,
    Json(question): Json<Question>,
) -> Response {
    match query!(
        "UPDATE questions SET title = $1, content = $2, tags = $3 WHERE id = $4",
        question.title,
        question.content,
        question.tags,
        question_id
    ).execute(&state.db_pool).await
    {
        Ok(_) => StatusCode::OK.into_response(),
        Err(_) => QuestionBaseError::response(StatusCode::BAD_REQUEST, "Failed to update question".to_string()),
    }
}
