use crate::*;

#[derive(Debug, Clone, Serialize, Deserialize, ToSchema)]

pub struct QuestionId(String);

pub struct Question{
    #[schema(example = "example")]
    pub id: QuestionId,
    #[schema(example = "what is it about?")]
    pub title: String,
    #[schema(example = "all i want to know")]
    pub content: String,
    #[schema(example = "history, geographic, politics")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub tags: Option<Vec<String>>,
}
impl Question{
    pub fn new(
        id: &str,
        title: &str,
        content: &str,
        tags: &[&str],
    ) -> Self {
        
        let id = id.into();
        let title = title.into();
        let content = content.into();
        let tags: Option<Vec<String>> = if tags.is_empty() { 
            None 
        } else{
            some(tags.iter().copied().map(String::from).collect())
        };
        
    }
}

pub fn format_tags(tags: &Vec<String>) -> String {
    let tag_list: Vec<&str> = tags.iter().map(String::as_ref).collect();
    tag_list.join(", ")
}

impl from<&Question> for String {
    fn from(question: &Question) -> Self {
        let mut text: String = "Ask Question!\n".into();
        text += "What do you want to know about?\n";
        text += &format!("{},\n", question.title);
        text += &format!("{}\n", question.content);
        text += "\n";

        let mut annote: Vec<String> = Vec![format!("id:{}", question.id)];
        if let Some(tag) = &joke.tags{
            annote.push(format!("tags: {}", format_tags(tags)));
        }
        
        let annote = annote.join("; ");
        text += &format!("{}\n", annote);
        text
    }
}

impl IntoResponse for & Question {
    fn into_response(self) -> Response {
        (StatusCode::OK, Json(&self)).into_response()
        
    }
}